/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: bitmanip_unittest.cc
 *  Purpose	: Source File for bit manipulation library testing
 *	Notes	: This is integrated as a build test, a verbose test
 *			  and a standalone binary. There are approximately
 *			  17 Million test comparisons run. This may seem
 *			  extensive, But even a ARM SoC processor should be
 *			  capable of handling this.
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#include <limits.h>
#include <stdio.h>
#include <bitmanip/bitmanip.h>
#include <stdlib.h>
#include <time.h>
#include <gtest/gtest.h>

#define SIZE 10
TEST(BITMANIP, MACROS)
{
	unsigned char size = 8;
	unsigned int i = size / CHAR_BIT;
	EXPECT_EQ(1, i) << "BIT_CONTAINER_NUMBER Macro fails";
}

TEST(BITMANIP, CONTAINER_MANIPULATION) {
	
	//Create
	bit_array * ba = bit_array_create(SIZE);
	EXPECT_NE(ba, nullptr);
	
	//Resize
	bit_array_resize(ba, SIZE / 2);
	EXPECT_EQ(SIZE / 2, ba->size);
	
	//Destroy
	bit_array_destroy(ba);
}

TEST(BITMANIP, BIT_MODIFIER) {

	bit_array * ba = bit_array_create(SIZE);
	
	//High
	bit_array_high(ba);
	unsigned char last = UCHAR_MAX >> (CHAR_BIT - ((SIZE) % CHAR_BIT));
	for (int i = 0; i < ((SIZE-1) / CHAR_BIT); ++i)
	{
		EXPECT_EQ(UCHAR_MAX, ba->bits[i]);
	}

	EXPECT_EQ(last, ba->bits[(SIZE - 1) / CHAR_BIT]);
	
	//Low
	bit_array_low(ba);
	for (int i = 0; i <= (SIZE - 1) / CHAR_BIT; ++i)
	{
		EXPECT_EQ(0, ba->bits[i]);
	}

	//Set Bit
	EXPECT_EQ(0, bit_array_set_bit(ba, 1));
	EXPECT_EQ(2, ba->bits[0]);

	//Clear Bit
	EXPECT_EQ(0, bit_array_clear_bit(ba, 1));
	EXPECT_EQ(0, ba->bits[0]);

	//Set Range
	EXPECT_EQ(0, bit_array_set_range(ba, 0, 2));
	EXPECT_EQ(7, ba->bits[0]);

	EXPECT_EQ(0, bit_array_clear_range(ba, 0, 1));
	EXPECT_EQ(4, ba->bits[0]);

	bit_array_destroy(ba);
}

TEST(BITMANIP, BIT_TESTING)
{
	bit_array *ba = NULL;
	//is set of NULL
	EXPECT_EQ(-1, bit_array_is_set(ba, 8));
	//is clear of NULL
	EXPECT_EQ(-1, bit_array_is_clear(ba, 8));
	ba = bit_array_create(8);
	bit_array_set_bit(ba, 7);

	//is set of 1
	EXPECT_EQ(1, bit_array_is_set(ba, 7));
	//is set of 0
	EXPECT_EQ(0, bit_array_is_set(ba, 6));
	//is clear of 1
	EXPECT_EQ(0, bit_array_is_clear(ba, 7));
	//is clear of 0
	EXPECT_EQ(1, bit_array_is_clear(ba, 6));
	
	//bit_array_destroy(ba);
}

TEST(BITMANIP, COPYING)
{
	bit_array* ba = bit_array_create(32);
	bit_array* bb = bit_array_create(32);
	
	for (int i = 0; i < 4; ++i)
	{
		ba->bits[i] = (0xdeadbeef >> (8 * i)) & 0xff;
	}

	bit_array_copy(bb, ba);
	for (int i = 0; i < 4; ++i)
	{
		EXPECT_EQ(ba->bits[i], bb->bits[i]);
	}

	bit_array_destroy(ba);
	bit_array_destroy(bb);

}

TEST(BITMANIP, LOGICAL_OPERATIONS)
{
	bit_array* arrA = bit_array_create(8);
	bit_array* arrB = bit_array_create(8);
	bit_array* arrX = bit_array_create(8);
	bit_array_set_range(arrA, 0, 3);
	bit_array_set_range(arrB, 4, 7);
	
//==========================================================
//AND TESTING
	//Pre-Testing Clear Test
	EXPECT_EQ(0, arrX->bits[0]) << "Pre-test for and failed";

	//NULL returns 1
	EXPECT_EQ(1, bit_array_and(arrX, NULL, arrA));
	EXPECT_EQ(1, bit_array_and(arrX, arrB, NULL));

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_and(arrX, arrB, arrA));
	//0xf0 & 0x0f = 0x00
	EXPECT_EQ(0x00, arrX->bits[0]) << "\t\tlogical and (0x00) fails";
	bit_array_low(arrX);

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_and(arrX, arrA, arrA));
	//0xf0 & 0x0f = 0x00
	EXPECT_EQ(0x0f, arrX->bits[0]) << "\t\tlogical and (0x0f) fails";
	bit_array_low(arrX);

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_and(arrX, arrB, arrB));
	//0xf0 & 0x0f = 0x00
	EXPECT_EQ(0xf0, arrX->bits[0]) << "\t\tlogical and (0xf0) fails";
	bit_array_low(arrX);
//==========================================================
//OR TESTING
	//Pre-Testing Clear Test
	EXPECT_EQ(0, arrX->bits[0]) << "Pre-test for or failed";
	//NULL returns 1
	EXPECT_EQ(1, bit_array_or(arrX, NULL, arrA));
	EXPECT_EQ(1, bit_array_or(arrX, arrB, NULL));

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_or(arrX, arrA, arrA));
	//0xf0 | 0x0f = 0xff
	EXPECT_EQ(0x0f, arrX->bits[0]) << "\t\tlogical or (0x0f) fails";
	bit_array_low(arrX);

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_or(arrX, arrB, arrB));
	//0xf0 | 0x0f = 0xff
	EXPECT_EQ(0xf0, arrX->bits[0]) << "\t\tlogical or (0xf0) fails";
	bit_array_low(arrX);

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_or(arrX, arrB, arrA));
	//0xf0 | 0x0f = 0xff
	EXPECT_EQ(0xff, arrX->bits[0]) << "\t\tlogical or (0xff) fails";
	bit_array_low(arrX);
//==========================================================
//XOR TESTING
	//Pre-Testing Clear Test
	EXPECT_EQ(0, arrX->bits[0]) << "Pre-test for xor failed";

	//NULL returns 1
	EXPECT_EQ(1, bit_array_xor(arrX, NULL, arrA));
	EXPECT_EQ(1, bit_array_xor(arrX, arrB, NULL));

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_xor(arrX, arrB, arrA));
	//0xf0 ^ 0x0f = 0xff
	EXPECT_EQ(0xff, arrX->bits[0]) << "\t\tlogical xor (0xff) fails";
	bit_array_low(arrX);

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_xor(arrX, arrA, arrA));
	//0x0f ^ 0x0f = 0x00
	EXPECT_EQ(0x00, arrX->bits[0]) << "\t\tlogical xor (0x00) #1 fails";
	bit_array_low(arrX);

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_xor(arrX, arrB, arrB));
	//0x0f ^ 0x0f = 0x00
	EXPECT_EQ(0x00, arrX->bits[0]) << "\t\tlogical xor (0x00) #2 fails";
	bit_array_low(arrX);
//==========================================================
//Not TESTING
	//NULL returns 1
	EXPECT_EQ(1, bit_array_not(arrX, NULL));

	//Proper and returns 0
	EXPECT_EQ(0, bit_array_not(arrX, arrA));
	//(~0x0f) = 0xf0
	EXPECT_EQ(0xf0, arrX->bits[0]) << "\t\tlogical not (~0x0f) fails";
	bit_array_low(arrX);
	
	//Proper and returns 0
	EXPECT_EQ(0, bit_array_not(arrX, arrB));
	//(~0xf0) = 0x0f
	EXPECT_EQ(0x0f, arrX->bits[0]) << "\t\tlogical not (~0xf0) fails";
	bit_array_low(arrX);

	bit_array_destroy(arrA);
	bit_array_destroy(arrB);
	bit_array_destroy(arrX);
}

TEST(BITMANIP, SHIFT_AND_ROTARY)
{
	bit_array* arrLeSh;
	bit_array* arrRiSh;
	bit_array* arrLeRo;
	bit_array* arrRiRo;

	srand(time(0));
	
	int bitpos;
	int testpos;

	for (int size = 2; size <= 1024; size++)
	{
		arrLeSh = bit_array_create(size);
		arrRiSh = bit_array_create(size);
		arrLeRo = bit_array_create(size);
		arrRiRo = bit_array_create(size);

		for (int step = 1; step < size; step++)
		{
			bit_array_low(arrLeSh);
			bit_array_low(arrRiSh);
			bit_array_low(arrLeRo);
			bit_array_low(arrRiRo);

			bitpos = rand() % size;
			
			bit_array_set_bit(arrLeSh, bitpos);
			bit_array_set_bit(arrRiSh, bitpos);
			bit_array_set_bit(arrLeRo, bitpos);
			bit_array_set_bit(arrRiRo, bitpos);
			
			EXPECT_EQ(0, bit_array_shift_left(arrLeSh, step));
			EXPECT_EQ(0, bit_array_shift_right(arrRiSh, step));
			EXPECT_EQ(0, bit_array_rotate_left(arrLeRo, step));
			EXPECT_EQ(0, bit_array_rotate_right(arrRiRo, step));

			for (int i = 0; i < (size + 1) / 2; i++)
			{
				testpos = rand() % size;

				EXPECT_EQ((testpos == bitpos + step) ? 1 : 0, bit_array_is_set(arrLeSh, testpos))
					<< "\t\t\tTest position " << testpos << " for a " 
					<< size << "-Bit field failed for a left shift from " 
					<< bitpos << " by " << step << ".";
				EXPECT_EQ((testpos == bitpos - step) ? 1 : 0, bit_array_is_set(arrRiSh, testpos))
					<< "\t\t\tTest position " << testpos << " for a " 
					<< size << "-Bit field failed for a right shift from " 
					<< bitpos << " by " << step << ".";
				EXPECT_EQ((testpos == (bitpos + step) % size) ? 1 : 0, bit_array_is_set(arrLeRo, testpos))
					<< "\t\t\tTest position " << testpos << " for a " 
					<< size << "-Bit field failed for a left rotate from " 
					<< bitpos << " by " << step << ".";
				EXPECT_EQ((testpos == (size + bitpos - step) % size) ? 1 : 0, bit_array_is_set(arrRiRo, testpos))
					<< "\t\t\tTest position " << testpos << " for a " 
					<< size << "-Bit field failed for a right rotate from " 
					<< bitpos << " by " << step << ".";

				
			}

		}
	}
}
