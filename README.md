### What is this repository for? ###
# Bitmanip #
* ver: 0.0.5
* A n-length bit field manipulation library.
* This includes single bit and range testing, comparison and editing. 
* Containers use all necessary CRUD operations


# Canary #
* ver: 0.0.2
* Path recognition, comparison, and prediction
* Any N-dimensional, query-able, quantifiable data can be used.
    
### How do I get set up? ###

* Just run cmake on your machine to set up.
This library includes testing based off of Google Test 
(https://code.google.com/p/googletest/)
In order to do use this testing suite, CMake expects to find a folder for GTest containing the include and lib folders of GTest. please follow their instructions on building GTest.
It is reccomended as of Canary version 0.0.2 to disable pthreads and build for Release when using GTest.

* Configuration

     BUILD_SHARED_LIBS: This option does nothing at the moment. will be fleshed out to build the shared library files in future versions

    CMAKE_CONFIGURATION_TYPES: Recommended types to use are Release and RelWithDebugInfo

    CMAKE_INSTALL_PREFIX: Where to install the library and binaries built for the library.
    
    GTEST_ROOT: root folder containing gtest. I have found "/usr " works best on linux, given you have moved the appropriate files there.

    build_*: build that sub directory.

* Dependencies

    CMAKE 3.0.0

    GTEST 1.7.0

### Contribution guidelines ###

* Please email an admin or contact one on bitbucket to find out how to contribute.