/*
 * ===========0==================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: bitmanip.h
 *  Purpose : Header File for bit manipulation of arbitray
 *			  sized bit arrays
 *	Notes	: This will always be expanding to include further
 *			  functionality. The core is manipulation of data
 *			  BUT, all CRUD operations should be supported.
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *	
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable, 
 *			serializable data. This can be physical points or 
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#ifndef CANARY_BITMANIP_H
#define CANARY_BITMANIP_H

/*
 * ==============================================================
 * TYPE DEFINITIONS
 * ==============================================================
 */

struct small_bit_array;
struct large_bit_array;
typedef struct small_bit_array small_bit_array;
typedef struct large_bit_array large_bit_array;

#ifdef LARGESETS
typedef large_bit_array bit_array;
#else
typedef small_bit_array bit_array;
#endif
/* endif LARGESETS */
/* bit array contianer */
struct small_bit_array {
	unsigned int size;
	unsigned char *bits;
};

/* large bit array container (meta-container) */
struct large_bit_array
{
	small_bit_array * size;
	unsigned char * bits;
};

typedef enum {
	BIN = 2,
	QUA = 4,
	HEX = 16
} base;

/*
 * ==============================================================
 * Prototypes
 * ==============================================================
 */

#ifdef __cplusplus
extern "C"{
#endif

/* Container Manipulation Functions */
bit_array *bit_array_create(const unsigned int);
void bit_array_resize(bit_array *, const unsigned int);
void bit_array_destroy(bit_array *);

/* IO Functions */
void bit_array_read_string(bit_array *, const char *, base, int);
void bit_array_read_file(bit_array*, char const*, int);
void bit_array_write_string(const bit_array *const, char *, base);
void bit_array_write_file(const bit_array *const, char const*); 
void bit_array_dump_bits(const bit_array *const, FILE *, char);

/* Bit Modifier Functions */
void bit_array_high(bit_array *);
void bit_array_low(bit_array *);
int bit_array_set_bit(bit_array *, const unsigned int);
int bit_array_clear_bit(bit_array *, const unsigned int);
int bit_array_set_range(bit_array *, signed int, signed int);
int bit_array_clear_range(bit_array *, signed int, signed int);

/* Bit Testing Functions */
int bit_array_is_set(bit_array *const, const unsigned int);
int bit_array_is_clear(bit_array *const, const unsigned int);

/* Copy Functions */
void bit_array_copy(bit_array *, bit_array *const);

/* Logical Functions */
int bit_array_and(bit_array *const, bit_array *const, bit_array *const);
int bit_array_or(bit_array *const, bit_array *const, bit_array *const);
int bit_array_xor(bit_array *const, bit_array *const, bit_array *const);
int bit_array_not(bit_array *const, bit_array *const);

/* Bit Shift and Rotary Functions */
int bit_array_shift_left(bit_array *, const unsigned int);
int bit_array_shift_right(bit_array *, const unsigned int);
int bit_array_rotate_left(bit_array *, const unsigned int);
int bit_array_rotate_right(bit_array *, const unsigned int);

/* Bit Increment/Decrement Functions*/
int bit_array_increment(bit_array *);
int bit_array_decrement(bit_array *);

/* Bit Mathematical Functions */
int bit_array_add(bit_array *, bit_array *const, bit_array *const);
int bit_array_subtract(bit_array *, bit_array *const, bit_array *const);
int bit_array_multiply(bit_array *, bit_array *const, bit_array *const);
int bit_array_divide(bit_array *, bit_array *const, bit_array *const);

/* Bit Comparison Functions */
int bit_array_compare_bit(bit_array *const, bit_array *const, const unsigned int);
int bit_array_compare_array(bit_array *const, bit_array *const);

/* end c++ compat */
#ifdef __cplusplus
}
#endif

#endif
/* endif CANARY_BITMANIP_H */ 
