/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: narytree.h
 *  Purpose : Header File for Nary-Tree Implementation
 *	Notes	: 
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#ifndef CANARY_NARY_TREE_H
#define CANARY_NARY_TREE_H

/*
 * =============================================================
 * INCLUDE GUARDS
 * =============================================================
 */

/* pos.h */
#ifndef CANARY_POS_H
#include <canary/pos.h>
#endif

/* packing.h */
#ifndef CANARY_PACKING_H
#include <common/packing.h>
#endif

/* weighted.h */
#ifndef CANARY_WEIGHTED_H
#include <canary/weighted.h>
#endif

/*
 * =============================================================
 * TYPE DEFINTIONS
 * =============================================================
 */

struct nary_node;
struct nary_datum;

typedef struct nary_node nary_node;
typedef struct nary_datum nary_datum;
typedef nary_node* nary_list;

struct nary_node {
	int			childrenSize;
	pos 		*position;
	nary_datum	*data;
	nary_list	*children;
};

struct nary_datum {
	int id;
	weightfunctor eval;
};

/*
 * ==============================================================
 * Prototypes
 * ==============================================================
 */

void make_nary_node(nary_node *, pos *, nary_datum *, nary_list *, int);
void link_nary_node(nary_node *, nary_node *);

#endif
