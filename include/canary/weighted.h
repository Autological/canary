/*
 * =============================================================
 * =============================================================
 * 				Canary: Path recognition library
 *
 *	File : weighted.h
 *	Purpose : Header File for Weight functors.
 *	Notes	: A Weight Functor has the purpose of taking in data
 *			  Evaulating it and giving it a weight. Due to its
 *			  such generic purpose. The code is very generic.
 *			  It should take in a void pointer and return an
 *			  Int. Ints are used instead of floats to allow for 
 *			  array indices to be accessed directly from the
 *			  functor. Additional weight functors may defined 
 *			  and implemented in the library IFF they are passed
 *			  a void pointer. An example of such modification
 *			  can be found in weight_mod.h weight_mod.c and
 *			  weight_mod_impl.c
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#ifndef CANARY_WEIGHTED_H
#define CANARY_WEIGHTED_H 

typedef int(*weightfunctor)(void *);

#endif
