/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: tray.h
 *  Purpose	: Header File for tray datatype. 
 *	Notes	: A Tray is a node system made up of a single array.
 *			  this array contains nodes who in turn contain
 *			  bitmasks of the nodes they can access.
 *			  This allows for any node to be a starting and/or
 *			  and ending point. This is the heart of canary.
 *			  As such it is IMPERITAVE to become familiar with
 *			  and use this data structure for the library to 
 *			  be of any use. Direction to and from can be 
 *			  expressed with two bitmasks and is implemented
 *			  trough directional_tray.h and directional_tray.c
 *	Author	: Luke Smith
 *	Date	: March 24, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#ifndef CANARY_TRAY_H
#define CANARY_TRAY_H

/*
 * =============================================================
 * INCLUDE GUARDS
 * =============================================================
 */

/* bitmanip.h */
#ifndef CANARY_BITMANIP_H
#include <bitmanip/bitmanip.h>
#endif

/* pos.h */
#ifndef CANARY_POS_H
#include <canary/pos.h>
#endif

/* weighted.h */
#ifndef CANARY_WEIGHTED_H
#include <canary/weighted.h>
#endif

/*
 * =============================================================
 * TYPE DEFINTIONS
 * =============================================================
 */

/* Trays are represented by tray */
/* Their nodes, tray_node */
/* The nodes' data, tray_data */
struct tray;
struct tray_node;
struct tray_node;
typedef struct tray tray;
typedef struct tray_node tray_node;
typedef struct tray_data tray_data;

struct tray {
	unsigned int size;
	tray_node *nodes;
	bit_array *accessMasks;
};

struct tray_node {
	pos			*position;
	tray_data	*data;
};

struct tray_data {
	int id;
	weightfunctor eval;
};

/*
 * ==============================================================
 * Prototypes
 * ==============================================================
 */

void tray_create_node(tray_node *, pos *, int, tray_data *);
void tray_create(tray *, int, tray_node *, bit_array *);
void tray_insert_node(tray *, tray_node*);
void tray_remove_node(tray *, int);
void tary_link_nodes(tray *, int, int);
void tray_unlink_node(tray, int, int);
void tray_swap_nodes(tray*, int, int);

#endif
