/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: pos.h
 *  Purpose	: Header file for n-dimensional points in
 *			  euclidean space
 *	Notes	:
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#ifndef CANARY_POS_H
#define CANARY_POS_H 

struct pos;
typedef struct pos pos;

struct pos {
	int dimCount;
	int *dims;
};

#endif