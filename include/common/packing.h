/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: packing.h
 *  Purpose	: Struct packing macros
 *	Notes	:
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#ifndef CANARY_PACKING_H
#define CANARY_PACKING_H 

#if (defined __GNUC__ || defined __CYGWIN__ )
#define PACK ( __Decl__ ) __Decl__ __attribute__((__packed__))
#elif (defined _WIN32)
#define PACK ( __Decl__ ) __pragma( pack(push, 1) ) __Decl__ \
	__pragma( pack(pop) )
#else 
#define PACK ( __Decl__ ) __Decl__

#endif

#endif
