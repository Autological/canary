/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *	File	: sample_bitmanip-bitshift.c
 *	Purpose : Demonstrate the bit shifting and rotation
 *			  functions in the bitmanip library
 *	Notes	: 
 *	Author	: Luke Smith
 *	Date	: March 16, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <bitmanip/bitmanip.h>

void printArray(bit_array*);
#define BITLEN 7
int main(void)
{
	int error = 0;
	bit_array* arrL = bit_array_create(32);
	
	arrL->bits[3] = 0xde;
	arrL->bits[2] = 0xad;
	arrL->bits[1] = 0xbe;
	arrL->bits[0] = 0xef;

	printArray(arrL);
	printf("\n");
	do
	{
		error = bit_array_rotate_right(arrL, 8);
		printArray(arrL);
		printf("\n");
	} while (error == 0 && arrL->bits[0] != 0xef);
	printArray(arrL);
	printf("\n%d\n\n", error);

	printArray(arrL);
	printf("\n");
	do
	{
		error = bit_array_shift_right(arrL, 1);
		printArray(arrL);
		printf("\n");
	} while (error == 0 && arrL->bits[0] != 0x00);
	printArray(arrL);
	printf("\n%d\n\n", error);

	arrL->bits[3] = 0xca;
	arrL->bits[2] = 0xfe;
	arrL->bits[1] = 0xba;
	arrL->bits[0] = 0xbe;

	printArray(arrL);
	printf("\n");
	do
	{
		error = bit_array_rotate_left(arrL, 8);
		printArray(arrL);
		printf("\n");
	} while (error == 0 && arrL->bits[3] != 0xca);
	printArray(arrL);
	printf("\n%d\n\n", error);

	printArray(arrL);
	printf("\n");
	do
	{
		error = bit_array_shift_left(arrL, 1);
		printArray(arrL);
		printf("\n");
	} while (error == 0 && arrL->bits[3] != 0x00);
	printArray(arrL);
	printf("\n%d\n\n", error);



#ifdef _WIN32
	system("pause");
#endif

	return 0;
}

void printArray(bit_array* arr)
{
	bit_array_dump_bits(arr, NULL, '|');
	printf(" ");
}

