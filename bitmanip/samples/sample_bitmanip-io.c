/*
* =============================================================
* =============================================================
*				Canary: Path recognition library
*
*	File	: sample_bitmanip-io.c
*	Purpose : Demonstrate the string and file buffers
*	Notes	:
*	Author	: Luke Smith
*	Date	: March 16, 2015
*
* -------------------------------------------------------------
*
*	Canary:	A Library for path mutation, recognition,
*			and comparison of n-dimensional, queryable,
*			serializable data. This can be physical points or
*			somewhat more complex data sets.
*
* =============================================================
* =============================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <bitmanip/bitmanip.h>

void printArray(bit_array*);

int main(void)
{
	bit_array* arrL = bit_array_create(16);
	int i = 0;

	printArray(arrL);
	
	for (i = 1; i < 19; i++)
	{
		bit_array_read_string(arrL, "FEDCBA9876543210", HEX, i*4);
		printArray(arrL);
	}
	for (i = 4; i < 129; i += 4)
	{
		bit_array_read_file(arrL, "sample_bitmanip-io_1024_in.txt", i);
		printArray(arrL);
	}
	
#ifdef _WIN32
	system("pause");
#endif

	return 0;
}

void printArray(bit_array* arr)
{
	bit_array_dump_bits(arr, NULL, '|');
	printf("\n");
}

