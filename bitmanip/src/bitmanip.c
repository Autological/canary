/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: bitmanip.c
 *  Purpose	: Source File for bit manipulation of arbitray
 *			  sized bit arrays
 *	Notes	:
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

/*
 * ==============================================================
 * INCLUDES
 * ==============================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <sys/stat.h>
#include <bitmanip/bitmanip.h>

/*
 * ==============================================================
 * MACROS
 * ==============================================================
 */

#ifndef CHAR_BIT
#define CHAR_BIT 8
#endif

/* container number for specified bit */
#define BIT_CONTAINER_NUMBER(bit)	((bit) / CHAR_BIT)

/* position number for specified bit */
#define BIT_CONTAINER_POSITION(bit)	((bit) % CHAR_BIT)

/* position mask for specified bit */
#define BIT_CONTAINER_MASK(bit)		(1 << BIT_CONTAINER_POSITION((bit)))

/* Characters needed for n bits */
#define BITS_TO_CHARS(bits)			(((bits - 1) / CHAR_BIT) + 1)

/* MSB position of a character  */
#define	MSB_CHAR_POSITION			CHAR_BIT - 1

/* MSB mask of a character */
#define MSB_CHAR_MASK				(1 << (CHAR_BIT - 1))

/* full character mask */
#define CHAR_MASK					(unsigned char) (0-1)

/* full character mask to position n */
#define POSITION_MASK(n)			CHAR_MASK >> (CHAR_BIT - n)

/*
 * ==============================================================
 * FUNCTION DEFINITIONS
 * ==============================================================
 */

/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Container Manipulation Functions
 *		-: These serve as ways on maintaining size and existence
 *		   of bit_array's
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_create
 *		Purpose: create a bit array of n bits
 *		Args   : const unsigned int n
 *		Returns: bit_array *
 * --------------------------------------------------------------
 */

bit_array *bit_array_create(const unsigned int n)
{
	bit_array *returnarray = (bit_array*)malloc(sizeof(bit_array*));


	/* Pre-allocation check */
	if (n == 0)
	{
		return NULL;
	}

	/* Post-allocation check*/
	if (returnarray == NULL)
	{
		fprintf(stderr, "Out of memory: Creating arr");
		exit(EXIT_FAILURE);
	}

	/* assignment */
	returnarray->size = n;
	returnarray->bits = (unsigned char *)calloc(BITS_TO_CHARS(n), sizeof(unsigned char));

	/* post-assignment check */
	if (returnarray->bits == NULL)
	{
		free(returnarray);
		fprintf(stderr, "Out of memory. Creating bits");
		returnarray = NULL;
		exit(EXIT_FAILURE);
	}

	return returnarray;
}


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_resize 
 *		Purpose: resize a bit array to n bits. If n is smaller 
 *				 than the current bit size, the array is trimmed
 *				 from MSB down.
 *		Args   : bit_array *const arr, const unsigned int n
 *		Returns: void
 * --------------------------------------------------------------
 */
void bit_array_resize(bit_array * arr, const unsigned int n)
{
	/* Pre-allocation check */
	if (n == 0)
	{
		free(arr);
		arr = NULL;
		return;
	}

	/* assignment */
	arr->size = n;
	arr->bits = (unsigned char *)realloc(arr->bits, BITS_TO_CHARS(n) * sizeof(unsigned char));

	/* Post-assignment check */
	if (arr->bits == NULL)
	{
		free(arr);
		fprintf(stderr, "Out of memory. realloc'ing bits");
		arr = NULL;
		exit(EXIT_FAILURE);
	}
}


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_destroy
 *		Purpose: destroy a bit array. If arr is NULL, do nothing.
 *		Args   : bit_array *const arr
 *		Returns: void
 * --------------------------------------------------------------
 */
void bit_array_destroy(bit_array * arr)
{
	if (arr != NULL)
	{
		if (arr->bits != NULL)
		{
			free(arr->bits);
		}

		free(arr);
	}
}


/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		IO Functions
 *		-: These serve as ways of editing bit_arrays' raw
 *		   data, or debug them
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_read_string
 *		Purpose: read a string into a bit array
 *		Args   : bit_array *arr, const char *str,
 *				 base base;
 *		Returns: void 
 * --------------------------------------------------------------
 */
void bit_array_read_string(bit_array *arr, const char *str, base base, int bitlen)
{
	char *vals = NULL;
	char *pos = NULL;
	char val;
	char bitjmp = 0;
	char chomps = 0;
	char byte = 0;
	int size = strlen(str);
	int strpos = 0;
	int MSBits = BIT_CONTAINER_POSITION(bitlen);
	int i = 0;
	int j = 0;

	if (arr == NULL)
	{
		arr = bit_array_create(bitlen);
	}
	else
	{
		bit_array_resize(arr, bitlen);
		bit_array_low(arr);
	}

	switch (base)
	{
	case BIN:
		vals = "01";
		bitjmp = 1;
		break;
	case QUA:
		vals = "0123";
		bitjmp = 2;
		break;
	case HEX:
	default:
		vals ="0123456789ABCDEF";
		bitjmp = 4;
		break;
	}
	
	chomps = CHAR_BIT / bitjmp;
	strpos = size - ((bitlen - 1) / bitjmp) - 1;
	i = (bitlen - 1) / CHAR_BIT;
	j = ((bitlen - 1)/ bitjmp) % chomps;
	if (strpos < 0)
	{
		j -= (-1 * strpos) % chomps;
		i += strpos / chomps;
		strpos = 0;
	}
	for (; i >= 0; i--)
	{
		byte = 0;
		for (; j >= 0; j--)
		{	
			val = 0;
			pos = (strpos <= size) ? strchr(vals, str[strpos]): 0;
			val = pos ? pos - vals : 0;
			byte |= val << (j * bitjmp);
			strpos++;
		}
		if (MSBits && i == (bitlen - 1) / CHAR_BIT && bitjmp * size > bitlen)
		{
			byte &= POSITION_MASK(MSBits);
		}
		arr->bits[i] = byte;
		j = chomps - 1;
	}
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_read_file
 *		Purpose: read a file's contents into a bit array
 *		Args   : bit_array *arr, const char *filename,
 *				 int bitlen;
 *		Returns: void
 * --------------------------------------------------------------
 */
void bit_array_read_file(bit_array *arr, char const* filename, int bitlen)
{
	char vals[17] = "0123456789ABCDEF";
	char *pos = NULL;
	int curr = '0';
	char valLo;
	char valHi;
	char byte;
	char size;
	int strpos;
	int MSBits = BIT_CONTAINER_POSITION(bitlen);
	int i;
	int hiPred;
	struct stat fileStat;
	FILE * fp = fopen(filename, "r");


	if (arr == NULL)
	{
		arr = bit_array_create(bitlen);
	}
	else
	{
		bit_array_resize(arr, bitlen);
		bit_array_low(arr);
	}

	if (stat(filename, &fileStat) < 0)
	{
		return;
	}
	
	if (fp == NULL)
	{
		perror("Bitmanip is exiting on an error: ");
		exit(EXIT_FAILURE);
	}
	
	size = fileStat.st_size;
	strpos = size - ((bitlen - 1) / 4) - 1;
	i = (bitlen - 1) / CHAR_BIT;
	hiPred = ((bitlen - 1) / 4) % 2;
	if (strpos < 0)
	{
		hiPred -= (-1 * strpos) % 2;
		i += strpos / 2;
		strpos = 0;
	}

	fseek(fp, strpos, SEEK_SET);

	// First byte read and MSByte hardcoded for efficiency.
	//set High nibble if needed.
	curr = fgetc(fp);
	pos = (curr != EOF) ? strchr(vals, curr) : 0;
	valHi = (pos && hiPred) ? (pos - vals) << 4 : 0;
	
	//Set Low nibble
	curr = fgetc(fp);
	pos = (curr != EOF) ? strchr(vals, curr) : 0;
	valLo = pos ? pos - vals : 0;

	byte = valHi | valLo;

	if (MSBits && i == (bitlen - 1) / CHAR_BIT && 4 * size > bitlen)
	{
		byte &= POSITION_MASK(MSBits);
	}
	arr->bits[i] = byte;
	i--;

	for (; i >= 0; i--)
	{
		//Set High nibble
		curr = fgetc(fp);
		pos = (curr != EOF) ? strchr(vals, curr) : 0;
		valHi = pos ? (pos - vals) << 4 : 0;
		
		//Set Low nibble
		curr = fgetc(fp);
		pos = (curr != EOF) ? strchr(vals, curr) : 0;
		valLo = pos ? pos - vals : 0;

		
		arr->bits[i] = valHi | valLo;
	}

}


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_dump_bits
 *		Purpose: dump current array to a file
 *		Args   : bit_array *const arr, FILE *fp
 *		Returns: void 
 * --------------------------------------------------------------
 */
void bit_array_dump_bits(const bit_array *const arr, FILE *fp, char delim)
{
	signed int i,j;

	if (fp == NULL) fp = stdout;
	for (i = BIT_CONTAINER_NUMBER(arr->size-1); i >= 0; i--)
	{
		for (j = CHAR_BIT - 1; j >= 0; --j)
		{
			if ((unsigned)i == BIT_CONTAINER_NUMBER(arr->size - 1) && BIT_CONTAINER_POSITION(arr->size) != 0 && (unsigned)j >= BIT_CONTAINER_POSITION(arr->size))
			{
				fprintf(fp, "%c", (((arr->bits[i] >> j) & 0x01) == 1) ? '!' : 'X');
			}
			else
			{
				fprintf(fp, "%d", (arr->bits[i] >> j) & 0x01);
			}
		}
		if (delim != '\0')
		{
			fprintf(fp, "%c", delim);
		}
	}
}


/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Bit Modifier Functions
 *		-: These serve as ways of modifying a set of bits in a
 *		   a bit_array
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_high
 *		Purpose: Set all bits of a bit array to 1.
 *		Args   : bit_array *const arr
 *		Returns: void
 * --------------------------------------------------------------
 */
void bit_array_high(bit_array *arr)
{
	unsigned pos = BIT_CONTAINER_POSITION(arr->size);
	unsigned i;
	for (i = 0; i < BITS_TO_CHARS(arr->size); i++)
	{
		arr->bits[i] = CHAR_MASK;
	}
	if (pos != 0)
	{
		arr->bits[BIT_CONTAINER_NUMBER(arr->size - 1)] = CHAR_MASK >> (CHAR_BIT - pos);
	}
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_low
 *		Purpose: Set all bits of a bit array to 0.
 *		Args   : bit_array *const arr
 *		Returns: void
 * --------------------------------------------------------------
 */
void bit_array_low(bit_array *arr)
{
	unsigned int i;

	for (i = 0; i < BITS_TO_CHARS(arr->size); i++)
	{
		arr->bits[i] = 0;
	}

}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_set_bit
 *		Purpose: Sets a single bit of position n in a bit array
 *				 to 1.
 *		Args   : bit_array *const arr, const unsigned int n
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_set_bit(bit_array *arr, const unsigned int n)
{
	if (n > arr->size)
	{
		return 1;
	}

	arr->bits[BIT_CONTAINER_NUMBER(n)] |= BIT_CONTAINER_MASK(n);
	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_clear_bit
 *		Purpose: Sets a single bit of position n in a bit array
 *				 to 0.
 *		Args   : bit_array *const arr, const unsigned int n
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_clear_bit(bit_array * arr, const unsigned int n)
{
	if (n > arr->size)
	{
		return -1;
	}

	arr->bits[BIT_CONTAINER_NUMBER(n)] &= arr->bits[BIT_CONTAINER_NUMBER(n)] ^ BIT_CONTAINER_MASK(n);
	return 0;
}


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_set_range
 *		Purpose: Sets a single bit of positions n  to m
 *				  (inclusive) in a bit array to 1.
 *		Args   : bit_array *const arr, const unsigned int n
 *				 const unsigned int m
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_set_range(bit_array * arr, signed int n, signed int m)
{
	unsigned i;

	/* swap bounds if necessary */
	if (n > m)
	{
		n ^= m;
		m ^= n;
		n ^= m;
	}

	/* Test bounds */
	if ((unsigned)m >= arr->size || n < 0)
	{
		return 1;
	}
	for (i = n; i <= (unsigned)m; i++)
	{
		arr->bits[BIT_CONTAINER_NUMBER(i)] |= BIT_CONTAINER_MASK(i);
	}

	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_clear_range
 *		Purpose: Sets a single bit of positions n  to m
 *				  (inclusive) in a bit array to 0.
 *		Args   : bit_array *const arr, const unsigned int n
 *				 const unsigned int m
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_clear_range(bit_array * arr, signed int n, signed int m)
{
	unsigned i;

	/* swap bounds if necessary */
	if (n > m)
	{
		n ^= m;
		m ^= n;
		n ^= m;
	}

	/* Test bounds */
	if ((unsigned)m >= arr->size || n < 0)
	{
		return 1;
	}
	for (i = n; i <= (unsigned)m; i++)
	{
		arr->bits[BIT_CONTAINER_NUMBER(i)] &= arr->bits[BIT_CONTAINER_NUMBER(i)] ^ BIT_CONTAINER_MASK(i);
	}

	return 0;
}

/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Bit Testing Functions
 *		-: These serve as ways of testing a bit in a bit_array
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_is_set
 *		Purpose: Tests if bit n of a bit array is set
 *		Args   : bit_array *const arr, const unsigned int n
 *		Returns: int;
 *				-1: arr is NULL;
 *				 0: bit is not set;
 *				 1: bit is set;
 * --------------------------------------------------------------
 */
int bit_array_is_set(bit_array *const arr, const unsigned int n)
{
	if (arr == NULL)
	{
		return -1;
	}
	return ((arr->bits[BIT_CONTAINER_NUMBER(n)] & BIT_CONTAINER_MASK(n)) >> BIT_CONTAINER_POSITION(n));
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_is_clear
 *		Purpose: Tests if bit n of a bit array is cleared
 *		Args   : bit_array *const arr, const unsigned int n
 *		Returns: int;
 *				-1: arr is NULL;
 *				 0: bit is not cleared;
 *				 1: bit is cleared;
 * --------------------------------------------------------------
 */
int bit_array_is_clear(bit_array *const arr, const unsigned int n)
{
	if (arr == NULL)
	{
		return -1;
	}
	return (~arr->bits[BIT_CONTAINER_NUMBER(n)] & BIT_CONTAINER_MASK(n)) >> BIT_CONTAINER_POSITION(n);
}

/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Copy Functions
 *		-: These serve as ways of copying an array
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_copy
 *		Purpose: copies bit array from src to dest
 *		Args   : bit_array *dest, bit_array *const src
 *		Returns: void
 * --------------------------------------------------------------
 */
void bit_array_copy(bit_array *dest, bit_array *const src)
{
	unsigned i;	

	if (dest == NULL)
	{
		fprintf(stderr, "Out of Memory. Copying Array");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < BITS_TO_CHARS(dest->size); i++)
	{
		dest->bits[i] = src->bits[i];
	}
}
/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Logical Functions
 *		-: These serve as ways of logically manipulating bits
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_and
 *		Purpose: set the logical and  "&" of two constant bit\
 *				 arrays into a bit array pointer
 *		Args   : bit_array *dest, bit_array *const lhs,
 *				 bit_array *const rhs
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_and(bit_array *const dest, bit_array *const lhs, bit_array *const rhs)
{
	unsigned Ma;
	unsigned Mi;
	unsigned i;

	if (lhs == NULL || rhs == NULL)
	{
		return 1;
	}

	Ma = (lhs->size < rhs->size) ? rhs->size : lhs->size;
	Mi = (lhs->size > rhs->size) ? rhs->size : lhs->size;
		
	if (Ma == 0 || Mi == 0)
	{
		return 1;
	}

	for (i = 0; i < BITS_TO_CHARS(Mi); i++)
	{
		dest->bits[i] = lhs->bits[i] & rhs->bits[i];
	}
	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_or
 *		Purpose: set the logical OR "|" of two constant bit
 *				 arrays into a bit array pointer
 *		Args   : bit_array *dest, bit_array *const lhs,
 *				 bit_array *const rhs
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_or(bit_array *const dest, bit_array *const lhs, bit_array *const rhs)
{
	unsigned Ma;
	unsigned Mi;
	unsigned i;
	bit_array* map;
	bit_array* mip;

	if (lhs == NULL || rhs == NULL)
	{
		return 1;
	}

	Ma = (lhs->size < rhs->size) ? rhs->size : lhs->size;
	Mi = (lhs->size > rhs->size) ? rhs->size : lhs->size;
	map = (lhs->size < rhs->size) ? rhs : lhs;
	mip = (map == lhs) ? rhs : lhs;


	if (Ma <= 0 || Mi <= 0)
	{
		return 1;
	}

	bit_array_copy(dest, map);

	for (i = 0; i < BITS_TO_CHARS(Mi); i++)
	{
		dest->bits[i] |= mip->bits[i];
	}
	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_xor
 *		Purpose: set the logical XOR "^" of two constant bit
 *				 arrays into a bit array pointer
 *		Args   : bit_array *dest, bit_array *const lhs,
 *				 bit_array *const rhs
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_xor(bit_array *const dest, bit_array *const lhs, bit_array *const rhs)
{
	unsigned Ma;
	unsigned Mi;
	unsigned i;
	bit_array* map; 
	bit_array* mip;

	if (lhs == NULL || rhs == NULL)
	{
		return 1;
	}
	
	Ma = (lhs->size < rhs->size) ? rhs->size : lhs->size;
	Mi = (lhs->size > rhs->size) ? rhs->size : lhs->size;
	map = (lhs->size < rhs->size) ? rhs : lhs;
	mip = (map == lhs) ? rhs : lhs;

	if (Ma <= 0 || Mi <= 0)
	{
		return 1;
	}

	bit_array_copy(dest, map);

	for (i = 0; i < BITS_TO_CHARS(Mi); i++)
	{
		dest->bits[i] ^= mip->bits[i];
	}
	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_not
 *		Purpose: set the logical NOT "~" of a constant bit
 *				 array into a bit array pointer
 *		Args   : bit_array *dest, bit_array *const src,
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_not(bit_array *const dest, bit_array *const src)
{
	unsigned i;

	if (src == NULL)
	{
		return 1;
	}

	for (i = 0; i < BIT_CONTAINER_NUMBER(src->size); i++)
	{
		dest->bits[i] = src->bits[i] ^ UCHAR_MAX;
	}
	return 0;
}

/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Bit Shift and Rotary Functions
 *		-: These serve as ways of shifting/rotating an array
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_shift_left
 *		Purpose: Shift the value of a bit array left, filling n 
 *				 LSBits to 0. 
 *		Args   : bit_array *arr, const unsigned int n
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_shift_left(bit_array* arr, const unsigned int n)
{
	unsigned char carry = 0, tmp;
	unsigned l = BIT_CONTAINER_NUMBER(n);
	int i, is = BIT_CONTAINER_NUMBER(arr->size-1);
	unsigned nn = n - l * CHAR_BIT;

	if (n == 0 || arr == NULL)
	{
		return 1;
	}

	if (l != 0) {
		for (i = is; i > 0; i--)
		{
			if (i < (signed)l )
			{
				arr->bits[i] = 0;
			}
			else
			{
				arr->bits[i] = arr->bits[i - (signed)l];
			}
		}
		arr->bits[0] = 0;
	}
	if (BIT_CONTAINER_POSITION(n) != 0) {
		for (i = 0; i <= is; i++)
		{
			tmp = arr->bits[i] >> (CHAR_BIT - nn);
			arr->bits[i] = (arr->bits[i] << nn) | carry;
			carry = tmp;
		}
	}
	if (BIT_CONTAINER_POSITION(arr->size) != 0)
	{
		arr->bits[BIT_CONTAINER_NUMBER(arr->size - 1)] &= CHAR_MASK >> (CHAR_BIT - BIT_CONTAINER_POSITION(arr->size));
	}

	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_shift_right
 *		Purpose: Shift the value of a bit array right, filling n 
 *				 MSBits to 0. 
 *		Args   : bit_array *arr, const unsigned int n
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_shift_right(bit_array* arr, const unsigned int n)
{
	unsigned char carry = 0, tmp;
	unsigned l = BIT_CONTAINER_NUMBER(n);
	signed int i, is = BIT_CONTAINER_NUMBER(arr->size-1);
	unsigned nn = n - l * CHAR_BIT;


	if (n == 0 || arr == NULL)
	{
		return 1;
	}

	if (l != 0) {
		for (i = 0; i <= is; i++)
		{
			if (i + l > (unsigned)is)
			{
				arr->bits[i] = 0;
			}
			else
			{
				arr->bits[i] = arr->bits[i + l];
			}
		}
	}
	if (BIT_CONTAINER_POSITION(n) != 0) {
		for (i = is; i >= 0; i--)
		{
			tmp = arr->bits[i] << (CHAR_BIT - nn);
			arr->bits[i] = (arr->bits[i] >> nn) | carry;
			carry = tmp;
		}
	}

	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_rotate_left
 *		Purpose: Shift the value of a bit array left, filling n 
 *				 LSBits with the original n MSBits. 
 *		Args   : bit_array *arr, const unsigned int n
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_rotate_left(bit_array* arr, const unsigned int n)
{
	unsigned char carry;
	unsigned char tmp;
	unsigned int m = n % arr->size;
	unsigned l = BIT_CONTAINER_NUMBER(m);
	signed int i;
	signed int is = BIT_CONTAINER_NUMBER(arr->size - 1);
	unsigned nn = m - (l * CHAR_BIT);
	unsigned rcarry = BIT_CONTAINER_POSITION(arr->size);
	unsigned rpush = CHAR_BIT - rcarry;
	unsigned char lmask = CHAR_MASK << nn;
	unsigned char rmask = CHAR_MASK >> (CHAR_BIT - nn);
	bit_array* copy = bit_array_create(arr->size);

	if (n == 0 || arr == NULL)
	{
		return 1;
	}

	
	if (l != 0) {
		bit_array_copy(copy, arr);
		for (i = 0; i <= is; i++)
		{
			arr->bits[i] = copy->bits[(BITS_TO_CHARS(arr->size) + i - l) % BITS_TO_CHARS(arr->size)];
		}
	}
	if (nn != 0)
	{
		bit_array_copy(copy, arr);
		carry = (copy->bits[is] & CHAR_MASK) >> (CHAR_BIT - nn);
		for (i = 0; i <= is; i++)
		{
			tmp = (copy->bits[i] & CHAR_MASK) >> (CHAR_BIT - nn);
			arr->bits[i] = (copy->bits[i] << nn) | carry;
			carry = tmp;
		}
		
	}
	if (rcarry != 0 && m != 0)
	{
		bit_array_copy(copy, arr);
		carry = copy->bits[is] >> rcarry;
		for (i = 0; i <= (signed)l-1; i++)
		{
			tmp = copy->bits[i] >> rcarry;
			arr->bits[i] = (copy->bits[i] << rpush) | carry;
			carry = tmp;
		}
		arr->bits[l] = (copy->bits[l] & lmask) | ((copy->bits[l] << rpush) & rmask) | carry;
	}
	if (rcarry != 0)
	{
		arr->bits[is] &= POSITION_MASK(rcarry);
	}
	return 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_rotate_right
 *		Purpose: Shift the value of a bit array right, filling n 
 *				 MSBits with the original n LSBits. 
 *		Args   : bit_array *arr, const unsigned int n
 *		Returns: int;
 *				 0: Success;
 *				 1: Failure;
 * --------------------------------------------------------------
 */
int bit_array_rotate_right(bit_array* arr, const unsigned int n)
{
	unsigned char carry;
	unsigned char tmp;
	unsigned int m = n % arr->size;
	unsigned l = BIT_CONTAINER_NUMBER(m);
	int i;
	int is = BIT_CONTAINER_NUMBER(arr->size - 1);
	unsigned nn = m - (l * CHAR_BIT);
	unsigned rcarry = BIT_CONTAINER_POSITION(arr->size);
	unsigned rpull = CHAR_BIT - rcarry;
	unsigned char lmask = CHAR_MASK << ((nn >= rcarry) ? (CHAR_BIT + rcarry - nn) : (rcarry - nn));
	unsigned char rmask = ~lmask & CHAR_MASK;
	int ie = (nn > rcarry) ? l + 1 : l;
	bit_array* copy = bit_array_create(arr->size);

	if (n == 0 || arr == NULL)
	{
		return 1;
	}


	if (l != 0) {
		bit_array_copy(copy, arr);
		for (i = 0; i <= is; i++)
		{
			arr->bits[i] = copy->bits[(BITS_TO_CHARS(arr->size) + i + l) % BITS_TO_CHARS(arr->size)];
		}
	}
	if (nn != 0)
	{
		bit_array_copy(copy, arr);
		carry = copy->bits[0] << (CHAR_BIT - nn);
		for (i = is; i >= 0; i--)
		{
			tmp = copy->bits[i] << (CHAR_BIT - nn);
			arr->bits[i] = (copy->bits[i] >> nn) | carry;
			carry = tmp;
		}
	}
	if (rcarry != 0 && m != 0)
	{
		bit_array_copy(copy, arr);
		carry = 0;
		for (i = 0; i < ie; i++)
		{
			tmp = copy->bits[is-i] << rcarry;
			arr->bits[is-i] = (copy->bits[is-i] >> rpull) | carry;
			carry = tmp;
		}
		if (is != 0)
		{
			lmask = (nn == rcarry) ? CHAR_MASK : lmask;
			rmask = ~lmask & CHAR_MASK;
			arr->bits[is - ie] = ((copy->bits[is-ie] >> rpull) & lmask) | (copy->bits[is-ie] & rmask) | carry;
		}
		else
		{
			arr->bits[0] = ((copy->bits[0] >> rpull) & lmask) | (copy->bits[0] & rmask);
		}
	}

	return 0;
}

/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Bit Increment/Decrement Functions
 *		-: These serve as single bit increment/decrement.
 *		   Such are useful for counters.
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_increment
 *		Purpose: Increment the bit array by one.
 *		Args   : bit_array* arr
 *		Returns: Int corresponding to overflow bit;
 * --------------------------------------------------------------
 */
int bit_array_increment(bit_array* arr)
{
	unsigned char carry = 1;
	unsigned int i;
	unsigned int pos = BIT_CONTAINER_POSITION(arr->size);
	unsigned int ie = BIT_CONTAINER_NUMBER(arr->size - 1);

	for (i = 0; 1 == carry && i <= ie; i++)
	{
		arr->bits[i] += carry;
		carry = (arr->bits[i] == 0) ? 1 : 0;
	}
	if (pos != 0)
	{
		arr->bits[ie] &= POSITION_MASK(pos);
	}
	return (arr->bits[ie] == 0) ? 1 : 0;
}

/*
 * --------------------------------------------------------------
 *		Name   : bit_array_decrement
 *		Purpose: Decrement the bit array by one
 *		Args   : bit_array* arr
 *		Returns: Int corresponding to underflow bit.
 * --------------------------------------------------------------
 */
int bit_array_decrement(bit_array* arr)
{
	unsigned char carry = 1;
	unsigned int i;
	unsigned int pos;
	unsigned int ie = BIT_CONTAINER_NUMBER(arr->size - 1);

	for (i = 0; 1 == carry && i <= ie; i++)
	{
		arr->bits[i] -= carry;
		carry = (arr->bits[i] == CHAR_MASK) ? 1 : 0;
	}
	if ((pos = BIT_CONTAINER_POSITION(arr->size)) != 0)
	{
		arr->bits[ie] &= POSITION_MASK(pos);
	}
	return (arr->bits[ie] == POSITION_MASK(pos)) ? 1 : 0;
}

/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Bit Mathematical Functions
 *		-: These serve as basic binary mathematical operators
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_add
 *		Purpose: Add two bit arrays and store result in third
 *				 bit array.
 *		Args   : bit_array *sum, bit_array *const lha,
 *				 bit_array *const rha
 *		Returns: Int;
 * 				 0: success;
 *				 1: failure;			
 * --------------------------------------------------------------
 */
int bit_array_add(bit_array *sum, bit_array *const lha, bit_array *const rha)
{
	
	int Ma;
	int Mi;
	int carry = 0;
	
	Ma = (lha->size < rha->size) ? rha->size : lha->size; 
	
	if(sum != NULL)
	{
		sum = bit_array_create(Ma);
	}
	else
	{
		bit_array_low(sum);
		bit_array_resize(sum, Ma);
	}
	return 0;
}
/*
 * --------------------------------------------------------------
 *		Name   : bit_array_subtract
 *		Purpose:
 *		Args   :
 *		Returns:
 * --------------------------------------------------------------
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_multiply
 *		Purpose:
 *		Args   :
 *		Returns:
 * --------------------------------------------------------------
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_divide
 *		Purpose:
 *		Args   :
 *		Returns:
 * --------------------------------------------------------------
 */


/*
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Bit Comparison Functions
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_compare_bit
 *		Purpose:
 *		Args   :
 *		Returns:
 * --------------------------------------------------------------
 */


/*
 * --------------------------------------------------------------
 *		Name   : bit_array_compare_array
 *		Purpose:
 *		Args   :
 *		Returns:
 * --------------------------------------------------------------
 */

