#include <canary/pos.h>
#include <canary/weighted.h>
#include <canary/narytree.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>



int idC;

int testEval(void * v);
void createDatum(nary_datum * d);
void printpostorder(nary_node *n);

int main(void)
{
	int i,j;
	nary_node *nodes;
	nary_datum *data;
	pos position;
	position.dimCount = 3;
	position.dims = (int*)calloc(position.dimCount,  sizeof(int));
	
	srand(time(NULL));

	nodes = calloc(21, sizeof (nary_node));
	data = calloc(21, sizeof(nary_datum));

	for (i = 0; i < 21; ++i)
	{
		createDatum(&data[i]);
		for(j = 0; j < position.dimCount; ++j) {
			position.dims[j] = rand() % 100;
		} 
		make_nary_node(&nodes[i], &position, &data[i], NULL, 0);
	}
	for (i = 1; i < 21; i+=4)
	{
		link_nary_node(&nodes[0], &nodes[i]);
		for (j = 1; j <= 3; ++j)
		{
			link_nary_node(&nodes[i], &nodes[i + j]);
		}
	}
	printpostorder(&nodes[0]);

#ifdef _WIN32
	system("pause");
#endif
/* _WIN32
*/
	return 0;
}


int testEval(void * v)
{
	nary_node n = *(nary_node*)v;
	int i, retVal=0;
	for (i = 0; i < n.position->dimCount; ++i)
		retVal += n.position->dims[i];
	return n.data->id*retVal/n.position->dimCount;
}

void createDatum(nary_datum * d)
{
	d->id = idC++;
	d->eval = testEval;
}
void printpostorder(nary_node *n)
{
	int i;
	if(n != NULL) 
	{
		printf("%d\n", n->data->eval((void*)n));
		for (i = 0; i < n->childrenSize; ++i)
		{
			printpostorder(n->children[i]);
		}
	}
	
}
