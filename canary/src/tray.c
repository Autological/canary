/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: tray.c
 *  Purpose	: Source File for Tray Data Structure
 *	Notes	: A Tray is a node system made up of a single array.
 *			  this array contains nodes who in turn contain
 *			  bitmasks of the nodes they can access.
 *			  This allows for any node to be a starting and/or
 *			  and ending point. This is the heart of canary.
 *			  As such it is IMPERITAVE to become familiar with
 *			  and use this data structure for the library to 
 *			  be of any use. Direction to and from can be 
 *			  expressed with two bitmasks and is implemented
 *			  trough directional_tray.h and directional_tray.c
 *	Author	: Luke Smith
 *	Date	: March 24, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */

/*
 * ==============================================================
 * INCLUDES
 * ==============================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <canary/pos.h>
#include <canary/weighted.h>
#include <canary/tray.h>
#include <bitmanip/bitmanip.h>

/*
 * ==============================================================
 * MACROS
 * ==============================================================
 */


/*
 * ==============================================================
 * FUNCTION DEFINITIONS
 * ==============================================================
 */

/*
 * --------------------------------------------------------------
 *		Name   : make_tray_node
 *		Purpose: create a node for use in a tray
 *		Args   : tray_node *node	- Node pointer to allocate
 *				 pos *p				- Position pointer
 *				 int psize			- Number of dims in p 
 *				 tray_data *data	- Tray datum array
 *		Returns: void
 * --------------------------------------------------------------
 */

void tray_create_node(tray_node *node, pos *p, int psize, tray_data *data)
{
	if (node)
	{
		free(node);
		node = NULL;
	}
	node = (tray_node *)malloc(sizeof(tray_node*));
	if (!node)
	{
		perror("Canary exiting on an error: ");
		exit(EXIT_FAILURE);
	}

	memcpy((void*)node->position, (void *)p, psize * sizeof(pos));
	node->data->id = data->id;
	node->data->eval = data->eval;
}

void tray_create(tray *tr, int csize, tray_node *nodes, bit_array *accessMasks)
{
	if (tr)
	{
		free(tr);
		tr = NULL;
	}
	tr = (tray *)malloc(sizeof(tray *));
	if (!tr)
	{
		perror("Canary exiting on an error: ");
		exit(EXIT_FAILURE);
	}
	memcpy((void*)tr->nodes, (void *)nodes, csize * sizeof(tray_node*));
	memcpy((void*)tr->accessMasks, (void *)accessMasks, csize * sizeof(bit_array));
}

void tray_insert_node(tray *tr, tray_node* node)
{
	int i;
	tray_node *tempNode;
	
	tempNode = (tray_node *)realloc(tr->nodes, (tr->size + 1) * sizeof(tray_node));
	if(!tempNode)
	{
		perror("Canary exiting on an error: ");
		exit(EXIT_FAILURE);
	}
	for(i = 0; i < tr->size; i++)
	{
		bit_arrayresize(&tr->accessMasks[i], (tr->size + 1));
	}
	memcopy(&tr->nodes[tr->size++], node, sizeof(node));
}

void tray_remove_node(tray *tr, int remNum)
{
	int i;
	tray_node *tempNode;
	bit_array *tempArray;
	
	if(remNum != tr->size - 1)
	{
		tray_swap_nodes(tr, remNum, tr->size - 1);
		bit_arraycopy(&tr->accessMasks[remNum], &tr->accessMasks[tr->size - 1]);
	
		for(i = 0; i < tr->size - 1; i++)
		{
			bit_arrayswap_bits(tr->accessMasks[i], remNum, tr->size - 1);
			bit_arraay_resize(tr->accessMasks[i], tr->size - 1);
		}
	}
	
	tempNode = (tray_node *)realloc(tr->nodes,  (tr->size - 1) * sizeof(tray_node));
	if(!tempNode)
	{
		perror("Canary exiting on an error: ");
		exit(EXIT_FAILURE);
	}
	tr->nodes = tempNode;
	
	tempArray = (bit_array *)realloc(tr->accessMasks, (tr->size - 1) * sizeof(bit_array));
	if(!tempArray)
	{
		perror("Canary exiting on an error: ");
		exit(EXIT_FAILURE);
	}
	tr->accessMasks = tempArray;
}
