/*
 * =============================================================
 * =============================================================
 *				Canary: Path recognition library
 *
 *  File	: narytree.c
 *  Purpose	: Source File for nary-tree data structure.
 *	Notes	: An N-ary tree is similar to a binary tree in that 
 *			  it contains a tree structure of nodes. What is 
 *			  different is the fact that a node MAY have up to n
 *			  Children, where N is predefined.
 *	Author	: Luke Smith
 *	Date	: March 17, 2015
 *
 * -------------------------------------------------------------
 *
 *	Canary:	A Library for path mutation, recognition,
 *			and comparison of n-dimensional, queryable,
 *			serializable data. This can be physical points or
 *			somewhat more complex data sets.
 *
 * =============================================================
 * =============================================================
 */


/*
 * ==============================================================
 * INCLUDES
 * ==============================================================
 */

#include <canary/pos.h>
#include <canary/weighted.h>
#include <canary/narytree.h>
#include <stdlib.h>
#include <string.h>

/*
 * ==============================================================
 * FUNCTION DEFINITIONS
 * ==============================================================
 */

void make_nary_node(nary_node * n, pos * p, nary_datum * datum, nary_list *list, int lsize)
{
	n->childrenSize = lsize;
	n->position = p;
	if(datum != NULL)
		n->data = datum;
	if(list != NULL)
		n->children = list;
}

void link_nary_node(nary_node * parent, nary_node * child)
{
	if(parent->childrenSize > 0)
		parent->children = (nary_list *) realloc(parent->children, (parent->childrenSize+1)*sizeof(nary_node));
	else
		parent->children = (nary_list *) malloc(sizeof(nary_node *));

	parent->children[parent->childrenSize] = child;
	++(parent->childrenSize);
}